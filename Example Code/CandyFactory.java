
public class CandyFactory{
	public Candy getCandy(String candyType){
	      if(candyType == null){
	         return null;
	      }		
	      if(candyType.equalsIgnoreCase("LEMON")){
	         return new Lemon();
	         
	      } else if(candyType.equalsIgnoreCase("CHOCOLATE")){
	         return new Chocolate();
	         
	      } else if(candyType.equalsIgnoreCase("MILK")){
	         return new Milk();
	      
		  } else if(candyType.equalsIgnoreCase("COKE")){
             return new Coke();
		  
		  } else if(candyType.equalsIgnoreCase("STRAWBERRY")){
			 return new Strawberry();
		  }
	      
	      return null;
	   }
}
