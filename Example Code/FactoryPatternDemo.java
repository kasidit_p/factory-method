
public class FactoryPatternDemo {
	public static void main(String[] args) {
	      CandyFactory candyFactory = new CandyFactory();

	      //get an object of Chocolate and call its taste method.
	      Candy candy1 = candyFactory.getCandy("CHOCOLATE");

	      //call taste method of Chocolate
	      candy1.taste();

	      //get an object of Coke and call its taste method.
	      Candy candy2 = candyFactory.getCandy("COKE");

	      //call taste method of Coke
	      candy2.taste();

	      //get an object of Lemon and call its taste method.
	      Candy candy3 = candyFactory.getCandy("LEMON");

	      //call taste method of Lemon
	      candy3.taste();
	      
	      //get an object of Strawberry and call its taste method.
	      Candy candy4 = candyFactory.getCandy("STRAWBERRY");

	      //call taste method of Strawberry
	      candy4.taste();

	      //get an object of Milk and call its taste method.
	      Candy candy5 = candyFactory.getCandy("MILK");

	      //call taste method of Milk
	      candy5.taste();
	}
}
