##Factory Pattern

The factory method pattern is a creational pattern which uses factory methods to deal with the problem of creating objects without specifying the exact class of object that will be created. This is done by creating objects via calling a factory method—either specified in an interface and implemented by child classes, or implemented in a base class and optionally overridden by derived classes—rather than by calling a constructor.

![uml](http://image.ohozaa.com/i/4b8/nub95n.png)

###Example

We're going to create a Candy interface and concrete classes implementing the Candy interface. A factory class CandyFactory is defined as a next step.

FactoryPatternDemo, our demo class will use CandyFactory to get a Candy object. It will pass information (CHOCOLATE / COKE / LEMON / MILK / STRAWBERRY) to CandyFactory to get the type of object it needs

####Step1

Create an interface.  [Candy.java](https://bitbucket.org/kasidit_p/factory-method/src/49f3f5e563a0b6df057a525827cd79ca372f72b4/Example%20Code/Candy.java?at=master)

    public interface Candy {
	    public void taste();
    }

####Step2


Create concrete classes implementing the same interface.

[Coke.java](https://bitbucket.org/kasidit_p/factory-method/src/49f3f5e563a0b6df057a525827cd79ca372f72b4/Example%20Code/Coke.java?at=master)

    public class Coke implements Candy{
	    public void taste() {
		    System.out.println("coke");
	    }
    }

[Chocolate.java](https://bitbucket.org/kasidit_p/factory-method/src/49f3f5e563a0b6df057a525827cd79ca372f72b4/Example%20Code/Chocolate.java?at=master)

    public class Chocolate implements Candy{
	    public void taste() {
		    System.out.println("chocolate");
	    }
    }


[Lemon.java](https://bitbucket.org/kasidit_p/factory-method/src/49f3f5e563a0b6df057a525827cd79ca372f72b4/Example%20Code/Lemon.java?at=master)

    public class Lemon implements Candy{
	    public void taste() {
		System.out.println("lemon");
	}
}

[Milk.java](https://bitbucket.org/kasidit_p/factory-method/src/49f3f5e563a0b6df057a525827cd79ca372f72b4/Example%20Code/Milk.java?at=master)

    public class Milk implements Candy{
	    public void taste() {
		    System.out.println("milk");
	    }
    }


[Strawberry.java](https://bitbucket.org/kasidit_p/factory-method/src/49f3f5e563a0b6df057a525827cd79ca372f72b4/Example%20Code/Strawberry.java?at=master)

    public class Strawberry implements Candy{
	    public void taste() {
		    System.out.println("strawberry");
	    }
    }

####Step 3
Create a Factory to generate object of concrete class based on given information.

[CandyFactory.java](https://bitbucket.org/kasidit_p/factory-method/src/49f3f5e563a0b6df057a525827cd79ca372f72b4/Example%20Code/CandyFactory.java?at=master)


    public class CandyFactory{
	    public Candy getCandy(String candyType){
	        if(candyType == null){
	             return null;
	        }		
	        if(candyType.equalsIgnoreCase("LEMON")){
	             return new Lemon();
	        } else if(candyType.equalsIgnoreCase("CHOCOLATE")){
	             return new Chocolate();
	        } else if(candyType.equalsIgnoreCase("MILK")){
	             return new Milk();
		    } else if(candyType.equalsIgnoreCase("COKE")){
                 return new Coke();
		    } else if(candyType.equalsIgnoreCase("STRAWBERRY")){
			     return new Strawberry();
		    }
	      return null;
	   }
    }

####Step 4
Use the Factory to get object of concrete class by passing an information such as type.

[FactoryPatternDemo.java](https://bitbucket.org/kasidit_p/factory-method/src/49f3f5e563a0b6df057a525827cd79ca372f72b4/Example%20Code/FactoryPatternDemo.java?at=master)


    public class FactoryPatternDemo {
	    public static void main(String[] args) {
	          CandyFactory candyFactory = new CandyFactory();

	        //get an object of Chocolate and call its taste method.
	        Candy candy1 = candyFactory.getCandy("CHOCOLATE");

	        //call taste method of Chocolate
	        candy1.taste();

	        //get an object of Coke and call its taste method.
	        Candy candy2 = candyFactory.getCandy("COKE");

	        //call taste method of Coke
	        candy2.taste();

	        //get an object of Lemon and call its taste method.
	        Candy candy3 = candyFactory.getCandy("LEMON");

	        //call taste method of Lemon
	        candy3.taste();
	      
	        //get an object of Strawberry and call its taste method.
	        Candy candy4 = candyFactory.getCandy("STRAWBERRY");

	        //call taste method of Strawberry
	        candy4.taste();

	        //get an object of Milk and call its taste method.
	        Candy candy5 = candyFactory.getCandy("MILK");

	        //call taste method of Milk
	        candy5.taste();
	    }
    }

####Step 5
Verify the output.

    chocolate
    coke
    lemon
    strawberry
    milk

##Exercise

Change the [CarFactory](https://bitbucket.org/kasidit_p/factory-method/src/437630ca4d7a079f4709f4574cf1c2286a7d4a54/Exercise/?at=master) class into factory pattern. Answer is in this [link](https://bitbucket.org/kasidit_p/factory-method/src/437630ca4d7a079f4709f4574cf1c2286a7d4a54/Exercise/Answer/?at=master).
