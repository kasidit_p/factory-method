
public class FactoryPattern {

	public static void main(String[] args) {
	      CarFactory carFactory = new CarFactory();

	      //get an object of Black and call its Color method.
	      Car car1 = carFactory.getCar("BLACK");

	      //call Color method of Black
	      car1.Color();

	      //get an object of White and call its Color method.
	      Car car2 = carFactory.getCar("WHITE");

	      //call Color method of White
	      car2.Color();

	      //get an object of Gold and call its Color method.
	      Car car3 = carFactory.getCar("GOLD");

	      //call Color method of Gold
	      car3.Color();
	      
	      //get an object of Silver and call its Color method.
	      Car car4 = carFactory.getCar("SILVER");

	      //call Color method of Silver
	      car4.Color();

	      //get an object of Blue and call its Color method.
	      Car car5 = carFactory.getCar("BLUE");

	      //call Color method of Blue
	      car5.Color();
	      
	      //get an object of Green and call its Color method.
	      Car car6 = carFactory.getCar("GREEN");

	      //call Color method of Green
	      car6.Color();
	      
	      //get an object of Yellow and call its Color method.
	      Car car7 = carFactory.getCar("YELLOW");

	      //call Color method of Yellow
	      car7.Color();
	}

}
