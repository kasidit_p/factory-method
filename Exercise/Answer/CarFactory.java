
public class CarFactory {

	public Car getCar(String candyType){
	      if(candyType == null){
	         return null;
	      }		
	      if(candyType.equalsIgnoreCase("BLACK")){
	         return new Black();
	         
	      } else if(candyType.equalsIgnoreCase("WHITE")){
	         return new White();
	         
	      } else if(candyType.equalsIgnoreCase("GOLD")){
	         return new Gold();
	      
		  } else if(candyType.equalsIgnoreCase("SILVER")){
             return new Silver();
		  
		  } else if(candyType.equalsIgnoreCase("BLUE")){
			 return new Blue();
		  
		  } else if(candyType.equalsIgnoreCase("RED")){
	         return new Red();
	         
		  } else if(candyType.equalsIgnoreCase("GREEN")){
	         return new Green();
	         
		  } else if(candyType.equalsIgnoreCase("YELLOW")){
	         return new Yellow();
		  }
	      
	      return null;
	   }

}
